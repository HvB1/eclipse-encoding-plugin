package org.hvb.gradle.eclipse.encoding;

import java.util.List;

import org.gradle.api.Project;
import org.gradle.api.provider.Provider

import spock.lang.Specification;

public class AbstractSpec extends Specification {
    
    boolean containsResource(Provider<Map<File,String>> p, Map<File, String> m) {
        def r1 = p.getOrElse([:]).collectEntries ({ k, v -> [(k.canonicalPath): v] })
        def r2 = m.collectEntries ({ k, v -> [(k.canonicalPath): v] })
        r1.intersect(r2) == r2
    }

    void equals(Provider<Map<File,String>> p, Map<File, String>... maps) {
        def r1 = p.getOrElse([:])
        def r2 = maps.collectEntries()
        assert r1.collectEntries ({ k, v -> [(k.canonicalPath): v] }) == r2.collectEntries ({ k, v -> [(k.canonicalPath): v] })
    }

	Map<File, String> encodingMap(Project p, String encoding, Object... resources) {
		p.files(resources).collectEntries({[(it.canonicalFile), encoding]})
	}
}
