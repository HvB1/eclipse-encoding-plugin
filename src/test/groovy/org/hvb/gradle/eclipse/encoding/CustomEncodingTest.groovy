package org.hvb.gradle.eclipse.encoding


import spock.lang.Specification
import spock.lang.TempDir

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

import java.nio.charset.Charset

import org.gradle.api.Project
import org.gradle.api.tasks.compile.GroovyCompile
import org.gradle.api.tasks.compile.JavaCompile
import org.gradle.testfixtures.ProjectBuilder

public class CustomEncodingTest extends AbstractSpec {
    Project project
    
    def setup() {
        def builder = ProjectBuilder.builder()
        project = builder.build()
    }
    
    def "default encoding is java default encoding"() {

        when:
        def encodings = new CustomEncoding(project)
        
        then:
        Charset.forName(encodings.defaultEncoding.get()) == Charset.defaultCharset()
    }    
    
    def "explicit default encoding using a string"() {

        given:
        def encoding = 'Latin9'
        def encodings = new CustomEncoding(project)

        when:
		def enc = encoding
        encodings.defaultEncoding enc

        then:
        Charset.forName(encodings.defaultEncoding.get()) == Charset.forName(encoding)
    }
    
    def "explicit default encoding using a Charset"() {

        given:
        def encoding = 'Latin9'
        def encodings = new CustomEncoding(project)

        when:
		def enc = Charset.forName(encoding)
        encodings.defaultEncoding enc

        then:
        Charset.forName(encodings.defaultEncoding.get()) == Charset.forName(encoding)
    }
        
    def "explicit default encoding using a string provider"() {

        given:
        def encoding = 'Latin9'
        def encodings = new CustomEncoding(project)

        when:
		def enc = project.provider({encoding})
        encodings.defaultEncoding enc

        then:
        Charset.forName(encodings.defaultEncoding.get()) == Charset.forName(encoding)
    }

	def "explicit default encoding using a Charset provider"() {

        given:
        def encoding = 'Latin9'
        def encodings = new CustomEncoding(project)

        when:
		def enc = project.provider({Charset.forName(encoding)})
        encodings.defaultEncoding enc

        then:
        Charset.forName(encodings.defaultEncoding.get()) == Charset.forName(encoding)
    }
    
    def "explicit project encoding using configuration closure"() {

        given:
        def encoding = 'Latin9'
        def encodings = new CustomEncoding(project)
        
        when:
        def closure = { defaultEncoding encoding }
        closure.delegate= encodings
        closure.call()
        
        then:
        Charset.forName(encodings.defaultEncoding.get()) == Charset.forName(encoding)
    }

	def "Resource encoding can be defined using a String for the encoding"() {

		given:
		def path = 'src/test/java'
		def enc = 'Latin9'
		def encodings = new CustomEncoding(project)
		
		when:
		def encoding = enc
        encodings.resourceEncoding (enc, path)

		then:
		def values = encodings.resources.getOrElse([:]).values() as Set
		values.size() == 1
		Charset.forName(values[0]) == Charset.forName(enc)
	}

	def "Resource encoding can be defined using a Charset"() {
		
		given:
		def path = 'src/test/java'
		def enc = 'Latin9'
		def encodings = new CustomEncoding(project)
		
		when:
		def encoding = Charset.forName(enc)
		encodings.resourceEncoding (enc, path)

		then:
		def values = encodings.resources.getOrElse([:]).values() as Set
		values.size() == 1
		Charset.forName(values[0]) == Charset.forName(enc)
	}
	
	def "Resource encoding can be defined using a String provider"() {
		
		given:
		def path = 'src/test/java'
		def enc = 'Latin9'
		def encodings = new CustomEncoding(project)
		
		when:
        def encoding = project.provider({enc})
		encodings.resourceEncoding (enc, path)

		then:
		def values = encodings.resources.getOrElse([:]).values() as Set
		values.size() == 1
		Charset.forName(values[0]) == Charset.forName(enc)
	}
	
	def "Resource encoding can be defined using a Charset provider"() {
		
		given:
		def path = 'src/test/java'
		def enc = 'Latin9'
		def encodings = new CustomEncoding(project)
		
		when:
        def encoding = project.provider({Charset.forName(enc)})
		encodings.resourceEncoding (enc, path)

		then:
		def values = encodings.resources.getOrElse([:]).values() as Set
		values.size() == 1
		Charset.forName(values[0]) == Charset.forName(enc)
	}
	
    def "Resource encoding files can be defined using a path"() {

        given:
        def encoding = 'Latin9'
        def path = 'src/test/java'
		def encodings = new CustomEncoding(project)
		
        when:
        def paths = path
        encodings.resourceEncoding (encoding, paths)

        then:
		def files = encodings.resources.getOrElse([:]).keySet()
        [project.file(path).canonicalFile] as Set ==files*.canonicalFile as Set
    }

    def "Resource encoding files can be defined using a file"() {

        given:
        def encoding = 'Latin9'
        def path = 'src/test/java'
		def encodings = new CustomEncoding(project)
		
        when:
        def paths = new File(path)
        encodings.resourceEncoding (encoding, paths)

        then:
		def files = encodings.resources.getOrElse([:]).keySet()
        [project.file(path).canonicalFile] as Set ==files*.canonicalFile as Set
    }

    def "Resource encoding files can be defined using a String provider"() {

        given:
        def encoding = 'Latin9'
        def path = 'src/test/java'
		def encodings = new CustomEncoding(project)
		
        when:
        def paths = project.provider({path})
        encodings.resourceEncoding (encoding, paths)

        then:
		def files = encodings.resources.getOrElse([:]).keySet()
        [project.file(path).canonicalFile] as Set ==files*.canonicalFile as Set
    }

    def "Resource encoding files can be defined using a File provider"() {

        given:
        def encoding = 'Latin9'
        def path = 'src/test/java'
		def encodings = new CustomEncoding(project)
		
        when:
        def paths = project.provider({new File(path)})
        encodings.resourceEncoding (encoding, paths)

        then:
		def files = encodings.resources.getOrElse([:]).keySet()
        [project.file(path).canonicalFile] as Set ==files*.canonicalFile as Set
    }

    def "Resource encoding files can be defined using an array"() {

        given:
        def encoding = 'Latin9'
        def path = 'src/test/java'
        def lpaths = [
            path,
            new File(path),
            project.provider({path}),
            project.provider({new File(path)})
		]
		def encodings = new CustomEncoding(project)
		
        when:
        def paths = lpaths.collect({[it].toArray()})[i]
        encodings.resourceEncoding (encoding, paths)

        then:
		def files = encodings.resources.getOrElse([:]).keySet()
        [project.file(path).canonicalFile] as Set ==files*.canonicalFile as Set

        where:
        i << (0..3)
    }

    def "Resource encoding files can be defined using iterable"() {

        given:
        def encoding = 'Latin9'
        def path = 'src/test/java'
        def lpaths = [
            path,
            new File(path),
            project.provider({path}),
            project.provider({new File(path)})
		]
		def encodings = new CustomEncoding(project)
		
        when:
        def paths = (lpaths.collect({[it]}))[i]
        encodings.resourceEncoding (encoding, paths)

        then:
		def files = encodings.resources.getOrElse([:]).keySet()
        [project.file(path).canonicalFile] as Set ==files*.canonicalFile as Set

        where:
        i << (0..3)
    }

    def "Resource encoding files can be defined using a list of paths, files, etc.."() {

        given:
        def encoding = 'Latin9'
        def (p1, p2, p3, p4, p5) = ['src/main/java','src/main/resources', 'src/test/java', 'src/test/resources', "build/libs" ]
        def l1 = [p1                    ,[p1, p2]      ,{[p1,p2]}        ,[p1]]
        def l2 = [p2 as File            ,[p3,[p4,[p5]]],[]               ,[p2,p3]]
        def l3 = [[p3, p4, p5].toArray(),[]            ,[p3,[p4,[],[p5]]],[[p4, [{p5}]].toArray()]]
		def encodings = new CustomEncoding(project)
		
        when:
        def o1 = l1[i]
        def o2 = l2[i]
        def o3 = l3[i]
        encodings.resourceEncoding (encoding, o1, o2, o3)

        then:
		def files = encodings.resources.getOrElse([:]).keySet()
		[p1, p2, p3, p4, p5].collect({project.file(it).canonicalFile}) as Set == files*.canonicalFile as Set
		
        where:
        i << (0..3)
    }

	def "resource encoding using method call"() {

		given:
		def encoding = 'Latin9'
        def path = 'src/test/java'
        def encodings = new CustomEncoding(project)
        
        when:
        encodings.resourceEncoding (encoding, path)

        then:
        equals(encodings.resources,
            encodingMap(project, 
                encoding, 
                path))
    }

    def "resource encoding with closure"() {

        given:
        def encoding = 'Latin9'
        def path = 'src/test/java'
        def encodings = new CustomEncoding(project)
        
        when:
        encodings.resourceEncoding (encoding) {
            from path
        }

        then:
        equals(encodings.resources,
            encodingMap(project, 
                encoding, 
                path))
    }
    def "resource encoding with closure and resource"() {

        given:
        def encoding = 'Latin9'
        def path1 = 'src/test1/java'
        def path2 = 'src/test2/java'
        def encodings = new CustomEncoding(project)
        
        when:
        encodings.resourceEncoding (encoding, path1){
            from path2
        }

        then:
        equals(encodings.resources,
            encodingMap(project, 
                encoding, 
                path2, path1))
    }
    def "resource encoding with JavaCompile"() {

        given:
        def encoding = 'Latin9'
        def path1 = 'src/main/java/org/test/some/java/file.java'
        def path2 = 'src/main/java/org/test/some/non/java/file.groovy'
        File f1 = project.file(path1)
        File f2 = project.file(path2)    
        f1.parentFile.mkdirs()
        f1.createNewFile()
        f2.parentFile.mkdirs()
        f2.createNewFile()
        def encodings = new CustomEncoding(project)
        project.tasks.register('compileJava', JavaCompile){
            source("src/main/java")
            options.encoding = encoding
        }
        
        when:
        encodings.fromCompiler(project.tasks.compileJava)

        then:
        equals(encodings.resources,
            encodingMap(project, 
                encoding, 
                f1))
        ! containsResource(encodings.resources,
            encodingMap(project, 
                encoding, 
                f2))
    }
    def "resource encoding with GroovyCompile"() {

        given:
        def encoding1 = 'Latin9'
        def encoding2 = 'Latin1'
        def path1 = 'src/main/java/org/test/some/java/file.java'
        def path2 = 'src/main/java/org/test/some/non/java/file.groovy'
        File f1 = project.file(path1)
        File f2 = project.file(path2)    
        f1.parentFile.mkdirs()
        f1.createNewFile()
        f2.parentFile.mkdirs()
        f2.createNewFile()
        def encodings = new CustomEncoding(project)
        project.tasks.register('compileGroovy', GroovyCompile) {
            source("src/main/java")
            options.encoding = encoding1
            groovyOptions.encoding = encoding2
        }
        
        when:
        encodings.fromCompiler(project.tasks.compileGroovy)
        
        then:
        equals(encodings.resources,
            encodingMap(project,
                encoding1,
                f1),
             encodingMap(project,
                encoding2,
                f2))
    }
    def "resource encoding with GroovyCompile Provider"() {

        given:
        def encoding1 = 'Latin9'
        def encoding2 = 'Latin1'
        def path1 = 'src/main/java/org/test/some/java/file.java'
        def path2 = 'src/main/java/org/test/some/non/java/file.groovy'
        File f1 = project.file(path1)
        File f2 = project.file(path2)    
        f1.parentFile.mkdirs()
        f1.createNewFile()
        f2.parentFile.mkdirs()
        f2.createNewFile()
        def encodings = new CustomEncoding(project)
        project.tasks.register('compileGroovy', GroovyCompile) {
            source("src/main/java")
            options.encoding = encoding1
            groovyOptions.encoding = encoding2
        }
        
        when:
        encodings.fromCompiler(project.tasks.named('compileGroovy'))
        
        then:
        equals(encodings.resources,
            encodingMap(project,
                encoding1,
                f1),
             encodingMap(project,
                encoding2,
                f2),
//             encodingMap(project,
//                null,
//                [])
             )
    }
    def "resource encoding with TaskCollection"() {

        given:
        def encoding1 = 'Latin9'
        def encoding2 = 'Latin1'
        def path1 = 'src/main/java/org/test/some/java/file.java'
        def path2 = 'src/test/groovy/org/test/some/non/java/file.groovy'
        File f1 = project.file(path1)
        File f2 = project.file(path2)    
        f1.parentFile.mkdirs()
        f1.createNewFile()
        f2.parentFile.mkdirs()
        f2.createNewFile()
        def encodings = new CustomEncoding(project)
        project.tasks.register('compileGroovy', GroovyCompile) {
            source("src/main/java")
            options.encoding = encoding1
            groovyOptions.encoding = encoding2
        }
        
        when:
        encodings.fromCompiler(project.tasks.withType(GroovyCompile))
        
        then:
        equals(encodings.resources,
            encodingMap(project,
                encoding1,
                f1)
             )
             
        when:
        project.tasks.register('compileTestGroovy', GroovyCompile) {
            source("src/test/groovy")
            options.encoding = encoding1
            groovyOptions.encoding = encoding2
        }
        
        then:
        equals(encodings.resources,
            encodingMap(project,
                encoding1,
                f1),
             encodingMap(project,
                encoding2,
                f2)
             )
    }
    def "resource encoding is live"() {

        given:
        def enc1 = 'ASCII'
        def enc2 = 'UTF-16'
        def encoding1 = project.objects.property(String.class).value(enc1)
        def encoding2 = 'Latin9'
        def encoding3 = 'Latin1'
        def d1 = 'src/main/java'
        def d2 = 'src/test/java'
        def d3 = 'build/generated/sources/sometask/groovy'
        def files = project.files(d1)
        def path11 = "${d1}/org/test/some/java/file.java"
        def path12 = "${d1}/org/test/some/non/java/file.groovy"
        def path2 = "${d2}/org/test/another/java/file.java"
        def path3 = "${d3}/org/another/non/java/file.groovy"
        def encodings = new CustomEncoding(project)
        project.tasks.register('compileGroovy', GroovyCompile)
        
        when:
        encodings.resourceEncoding(encoding1, files)
        
        then:
        equals(encodings.resources,
            encodingMap(project,
                enc1,
                d1))

        when:
        encodings.fromCompiler(project.tasks.compileGroovy)
        
        then:
        equals(encodings.resources,
            encodingMap(project,
                enc1,
                d1),
            encodingMap(project,
                null,
                []),
             encodingMap(project,
                'UTF-8',
                []))

        when:
        files.from(d2)
        project.tasks.named('compileGroovy').configure {
            source(d1)
            source(d2)
        }
        File f11 = project.file(path11)
        f11.parentFile.mkdirs()
        f11.createNewFile()
        
        then:
        equals(encodings.resources,
            encodingMap(project,
                enc1,
                d1, d2),
            encodingMap(project,
                null,
                path11),
             encodingMap(project,
                'UTF-8',
                []))

        when:
        files.from(d3)
        project.tasks.named('compileGroovy').configure {
            source(d3)
            options.encoding = encoding2
            groovyOptions.encoding = encoding3
        }        
        File f12 = project.file(path12)
        f12.parentFile.mkdirs()
        f12.createNewFile()
        File f2 = project.file(path2)
        f2.parentFile.mkdirs()
        f2.createNewFile()
        
        then:
        equals(encodings.resources,
            encodingMap(project,
                enc1,
                d1, d2, d3),
            encodingMap(project,
                encoding2,
                path11, path2),
             encodingMap(project,
                encoding3,
                path12))

        when:
        File f3 = project.file(path3)
        f3.parentFile.mkdirs()
        f3.createNewFile()
        encoding1.set(enc2)
        
        then:
        equals(encodings.resources,
            encodingMap(project,
                enc2,
                d1, d2, d3),
            encodingMap(project,
                encoding2,
                path11, path2 ),
             encodingMap(project,
                encoding3,
                path12, path3))
    }
}
