package org.hvb.gradle.eclipse.encoding

import java.nio.charset.Charset

import org.gradle.api.Project
import org.gradle.api.tasks.compile.GroovyCompile
import org.gradle.testfixtures.ProjectBuilder

import groovy.json.JsonGenerator.Options

class EclipseAlternateModelTest extends AbstractSpec {
    
    Project project
    
    def setup() {
        def builder = ProjectBuilder.builder()
        project = builder.build()
    }

    def "configure encodings with project.eclipse.encodings methods"() {
        
        given:
        def encoding1 = 'Latin9'
        def encoding2 = 'UTF-8'
        def d1 = 'src/main/java'
        def d2 = 'src/main/java'
        def path1 = "${d1}/org/test/some/java/file.java"
        def path2 = "${d1}/org/test/some/non/java/file.groovy"
        def path3 = "${d1}/org/test/another/resource/file.png"
        File f1 = project.file(path1)
        File f2 = project.file(path2)
        File f3 = project.file(path3)
        f1.parentFile.mkdirs()
        f1.createNewFile()
        f2.parentFile.mkdirs()
        f2.createNewFile()
        f3.parentFile.mkdirs()
        f3.createNewFile()
        project.tasks.register('compileGroovy', GroovyCompile){
            source(d1)
            options.encoding = encoding1
        }
        
        when: 
        project.ext.eclipse = project.objects.newInstance(EclipseAlternateModel.class)
        project.eclipse.encodings.resourceEncoding(encoding1, d1, d2) 
        project.eclipse.encodings.fromCompiler(project.tasks.compileGroovy)
        
        then:
        Charset.forName(project.eclipse.encodings.defaultEncoding.get()) == Charset.defaultCharset()
        equals(project.eclipse.encodings.resources,
            encodingMap(project,
                encoding1,
                d1, d2),
            encodingMap(project,
                encoding1,
                path1 ),
             encodingMap(project,
                encoding2,
                path2))
    }
    def "configure encodings with a closure"() {
        
        given:
        def encoding1 = 'Latin9'
        def encoding2 = 'UTF-8'
        def d1 = 'src/main/java'
        def d2 = 'src/main/java'
        def path1 = "${d1}/org/test/some/java/file.java"
        def path2 = "${d1}/org/test/some/non/java/file.groovy"
        def path3 = "${d1}/org/test/another/resource/file.png"
        File f1 = project.file(path1)
        File f2 = project.file(path2)
        File f3 = project.file(path3)
        f1.parentFile.mkdirs()
        f1.createNewFile()
        f2.parentFile.mkdirs()
        f2.createNewFile()
        f3.parentFile.mkdirs()
        f3.createNewFile()
        project.tasks.register('compileGroovy', GroovyCompile){
            source(d1)
            options.encoding = encoding1
        }
        
        when:
        project.ext.eclipse = project.objects.newInstance(EclipseAlternateModel.class)
        project.eclipse.encodings {
            resourceEncoding(encoding1, d1, d2)
            fromCompiler(project.tasks.compileGroovy)
        }
        
        then:
        Charset.forName(project.eclipse.encodings.defaultEncoding.get()) == Charset.defaultCharset()
        equals(project.eclipse.encodings.resources,
            encodingMap(project,
                encoding1,
                d1, d2),
            encodingMap(project,
                encoding1,
                path1 ),
             encodingMap(project,
                encoding2,
                path2))
    }
}
