package plugin

import org.gradle.testkit.runner.GradleRunner
import spock.lang.Specification
import spock.lang.TempDir

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS
import static org.gradle.testkit.runner.TaskOutcome.UP_TO_DATE

public class BuildWithEclipsePluginTest  extends Specification {
    @TempDir File testProjectDir
    //File testProjectDir = new File("/tmp/eclipseResourceEncoding/")
    File buildFile
    
    def setup() {
        testProjectDir.mkdir()
        buildFile = new File(testProjectDir, 'build.gradle')
        buildFile.text = 
        """plugins {
               id 'java'
               id 'eclipse'
               id 'eclipse-encoding'
           }
        """
    }

    def "can successfully configure eclipse encodings"() {
        buildFile << 
        """eclipse {
               // tasks to run on "Refresh Gradle Project"
               //synchronizationTasks([eclipseResourceEncoding, makeGeneratedDirs])
               // tasks to run when build automatically is enabled
               //autoBuildTasks([cup, jflex])
               encodings {
                   defaultEncoding 'UTF-8'
                   resourceEncoding("UTF-8", sourceSets.main.java.srcDirs)
                   resourceEncoding(compileJava.options.encoding, compileJava.source)
                   resourceEncoding("UTF-8") {
                       from('src/test')
                   }
               }
           }
        """

        when:
        def result = GradleRunner.create()
            .withProjectDir(testProjectDir)
            .withArguments('eclipseResourceEncoding')
            .withPluginClasspath()
            .build()

        then:
        result.task(":eclipseResourceEncoding").outcome == SUCCESS || result.task(":eclipseResourceEncoding").outcome == UP_TO_DATE
    }
}