package plugin


import spock.lang.Specification
import spock.lang.TempDir

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.hvb.gradle.eclipse.encoding.AbstractSpec

public class ConfigurationWithEclipsePluginTest extends AbstractSpec {
    @TempDir File testProjectDir
    File buildFile
    Project project
    
    def setup() {
        def builder = ProjectBuilder.builder()
        project = builder.build()
        project.apply(plugin: 'java')
        project.apply(plugin: 'eclipse')
    }
    
    def "java implicit default encoding for eclipse"() {

        when:
        project.apply(plugin: 'eclipse-encoding')
        
        then:
        project.eclipse.encodings.defaultEncoding.get() == 'UTF-8'
    }    
    
    def "explicit default encoding using configuration closure"() {

        given:
        def encoding = 'Latin9'

        when:
        project.apply(plugin: 'eclipse-encoding')
        project.eclipse {
            encodings.defaultEncoding encoding
        }

        then:
        project.eclipse.encodings.defaultEncoding.get() == encoding
    }
    
    def "explicit default encoding using configuration closures"() {

        given:
        def encoding = 'Latin9'

        when:
        project.apply(plugin: 'eclipse-encoding')
        project.eclipse {
            encodings {
                defaultEncoding encoding
            }
        }

        then:
        project.eclipse.encodings.defaultEncoding.get() == encoding
    }
        
    def "resource encoding"() {
 
        given:
        def encoding = 'Latin9'
        def path = 'src/test/java'
        
        when:
        project.apply(plugin: 'eclipse-encoding')
        project.eclipse.encodings.resourceEncoding (encoding, path)

        then:
        containsResource(project.eclipse.encodings.resources,
            encodingMap(project, 
                encoding, 
                path))
    }

    def "encoding with closure"() {

        given:
        def encoding = 'Latin9'
        def path = 'src/test/java'

        when:
        project.apply(plugin: 'eclipse-encoding')
        project.eclipse.encodings.resourceEncoding (encoding){
            from path
        }
        
        then:
        containsResource(project.eclipse.encodings.resources,
            encodingMap(project, 
                encoding, 
                path))
    }
    def "encoding with closure and resource"() {

        given:
        def encoding = 'Latin9'
        def path1 = 'src/test1/java'
        def path2 = 'src/test2/java'

        when:
        project.apply(plugin: 'eclipse-encoding')
        project.eclipse.encodings.resourceEncoding (encoding, path1){
            from path2
        }
        
        then:
        containsResource(project.eclipse.encodings.resources,
            encodingMap(project, 
                encoding, 
                path2, path1))
    }
    def "resource encoding with JavaCompile"() {

        given:
        def encoding = 'Latin9'
        def path1 = 'src/main/java/org/test/some/java/file.java'
        def path2 = 'src/main/java/org/test/some/non/java/file.groovy'
        File f1 = project.file(path1)
        File f2 = project.file(path2)    
        f1.parentFile.mkdirs()
        f1.createNewFile()
        f2.parentFile.mkdirs()
        f2.createNewFile()

        when:
        project.apply(plugin: 'eclipse-encoding')
        project.eclipse.encodings.fromCompiler(project.tasks.compileJava)
        project.tasks.compileJava {
            //source([path1, path2])
            options.encoding = encoding
        }

        then:
        containsResource(project.eclipse.encodings.resources,
            encodingMap(project, 
                encoding, 
                path1))
        ! containsResource(project.eclipse.encodings.resources,
            encodingMap(project, 
                encoding, 
                path2))
    }
    def "applying the plugin should create a 'eclipseResourceEncoding' Task "() {
        
        when:
        project.apply(plugin: 'eclipse-encoding')
        def myTask = project.tasks.named("eclipseResourceEncoding")
        
        then:
        myTask.present
    }
}
