package org.hvb.gradle.eclipse.encoding;

import org.gradle.api.NamedDomainObjectContainer
import org.gradle.api.Plugin
import org.gradle.api.Project

public class EclipseEncodingPlugin implements Plugin<Project> {
    //Project project
    @Override
    public void apply(Project project) {
        //this.project = project;
        project.with {
            ext.eclipse = objects.newInstance(EclipseAlternateModel.class)
            pluginManager.withPlugin('eclipse'){
            extensions.eclipse.extensions.add('encodings', ext.eclipse.encodings)
            // make sure extra property and extension for eclipse are the same
            ext.eclipse = extensions.eclipse
            def eclipseResourceEncoding = tasks.register('eclipseResourceEncoding')
            // encoding will be defined when eclipse task run
            tasks.named('eclipse'){ dependsOn(eclipseResourceEncoding) }
            // run the task on "Refresh Gradle Project"
            eclipse.synchronizationTasks(eclipseResourceEncoding)
            //eclipse.extensions.create('encodings', CustomEncoding.class)
            
            eclipseResourceEncoding.configure {
                ext.outputFile = file('.settings/org.eclipse.core.resources.prefs')
                ext.projectEncoding = eclipse.encodings.defaultEncoding.orElse(java.nio.charset.Charset.defaultCharset().name())
                
                //ext.encodings = provider({eclipse.encodings.resources.collectEntries {res -> def encoding = res.encoding.orNull ; res.files.collectEntries({ (true || it.exists()) ? [(it.canonicalFile): encoding] : [:]})}})
                ext.encodings = provider({eclipse.encodings.resources.getOrElse([:])})
                /*
                ext.encodings = provider ({
                    Properties eclipseEncodingProperties = new Properties(Collections.singletonMap('eclipse.preferences.version','1'))
                    if (outputFile.exists()) {
                        outputFile.withInputStream { eclipseEncodingProperties.load(it) }
                    }
                    eclipseEncodingProperties.put('encoding/<project>', projectEncoding.get())
                    eclipse.encodings.resources.each { res ->
                        def encoding = res.encoding.orNull
                        res.files.each { d -> 
                            def path = java.nio.file.FileSystems.default.getPath(projectDir.toURI().relativize(d.toURI()).toString())
                            def key = ((path.parent == null) ? 'encoding/' : 'encoding//') + projectDir.toURI().relativize(d.toURI()).toString().replaceFirst('/$','')
                            (encoding != null && d.exists()) ? eclipseEncodingProperties.put(key, encoding) : eclipseEncodingProperties.remove(key)
                        }
                    }
                    eclipseEncodingProperties
                })
                */
                
                inputs.property("file.encoding", projectEncoding)
                outputs.file(outputFile).withPropertyName('outputFile')
                inputs.property("resources encodings", encodings)
                 
                doLast {
                    Properties eclipseEncodingProperties = new Properties(Collections.singletonMap('eclipse.preferences.version','1'))
                    if (outputFile.exists()) {
                        outputFile.withInputStream { eclipseEncodingProperties.load(it) }
                    }
                    Properties eclipseEncodingProperties2 = eclipseEncodingProperties.clone()
                    Boolean modified = false
                    if (projectEncoding.present)
                        modified = (projectEncoding.get() != eclipseEncodingProperties.put('encoding/<project>', projectEncoding.get()))
                    else 
                        modified = (eclipseEncodingProperties.remove('encoding/<project>') != null)
                    encodings.getOrElse([:]).each { f, v -> println(f); println(v)}
                    encodings.getOrElse([:]).each { d, v -> 
                            def path = java.nio.file.FileSystems.default.getPath(projectDir.toURI().relativize(d.toURI()).toString())
                            def key = ((path.parent == null) ? 'encoding/' : 'encoding//') + projectDir.toURI().relativize(d.toURI()).toString().replaceFirst('/$','')
                            if (v != null && d.exists()) {
                                modified = modified ?: (v != eclipseEncodingProperties.put(key, v)) 
                            } else {
                                modified = modified ?: (eclipseEncodingProperties.remove(key) != null)
                            }
                    }
                    println "modified: "+modified
                    println "modified2: "+(! eclipseEncodingProperties.equals(eclipseEncodingProperties2))
                    if (! eclipseEncodingProperties.equals(eclipseEncodingProperties2))
                        outputFile.withOutputStream { eclipseEncodingProperties.store(it, 'generated by '+name) }
                    
                    //outputFile.withOutputStream { encodings.get().store(it, 'generated by '+name) }
                    //eclipseEncodingProperties.list(System.out)
                }
            }
        }
        }
    }
}
