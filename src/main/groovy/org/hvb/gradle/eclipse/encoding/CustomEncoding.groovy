package org.hvb.gradle.eclipse.encoding;

import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.TaskProvider
import org.gradle.api.tasks.compile.AbstractCompile
import org.gradle.api.tasks.compile.GroovyCompile
import org.gradle.api.tasks.compile.JavaCompile
import org.gradle.api.tasks.scala.ScalaCompile

import java.nio.charset.Charset

import javax.inject.Inject

import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.file.ConfigurableFileCollection
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.Property;
import org.gradle.api.provider.Provider

public class CustomEncoding {
    Property<String> defaultEncoding
    protected ListProperty<Map<File, String>> resources
    Project project

    @Inject
    public CustomEncoding(Project project) {
        this.project = project
        this.defaultEncoding = project.objects.property(String.class).value(java.nio.charset.Charset.defaultCharset().name())
        this.@resources = this.project.objects.listProperty(Map).value([])
    }

    public void defaultEncoding(Object encoding) {
        this.defaultEncoding.set(this.encoding(encoding))
    }
    public void resourceEncoding(Object encoding, Object... paths){
        Provider<String> encodingProvider = this.encoding(encoding)
        ConfigurableFileCollection files = project.files(paths)
        this.@resources.add(
            this.project.provider({
                files.collectEntries({[(it.canonicalFile): encodingProvider.orNull]})
            }))
    }
    public void resourceEncoding(Object encoding, Object paths, Closure closure){
        ConfigurableFileCollection files = project.files(paths, closure)
        this.resourceEncoding(encoding, files)
    }
    public void resourceEncoding(Object encoding, Closure closure){
        this.resourceEncoding(encoding, [], closure)
    }
    public void resourceEncoding(Object encoding, Object paths, Action<? super ConfigurableFileCollection> action){
        ConfigurableFileCollection files = project.files(paths, action)
        this.resourceEncoding(encoding, files)
    }
    public void resourceEncoding(Object encoding, Action<? super ConfigurableFileCollection> action){
        this.resourceEncoding(encoding, [], action)
    }
    public Provider<Map<File, String>> getResourcesEncodings() {
        this.project.provider({
            this.resources.collectEntries({[(it.canonicalFile): this.encoding.orNull]})
        })
    }
    
    public void fromCompiler(Object... compilers) {
        compilers.each { c-> 
            this.@resources.add(this.getSourceEncodings(c))
        }
    }
    
    public Provider<Map<File, String>> getResources() {
        def res = this.@resources.map({it.collectEntries()})
    }
    
    // methodes pour la gestion de l'encoding
    protected Provider<String> encoding(Object enc){
        return this.project.provider({enc})
    }
    protected Provider<String> encoding(Charset enc){
        return this.project.provider({enc.name()})
    }
    protected Provider<String> encoding(Provider p){
        return p.map({ (it instanceof Charset) ? ((Charset)it).name() : (String)it})
    }

    // methodes pour la gestion des compilateurs    
    protected Provider<Map<File, String>> getSourceEncodings(Object c) {
        project.provider({[:]})
    }
    protected Provider<Map<File, String>> getSourceEncodings(Iterable compilers) {
            print("compiler list: ")
            //print(c.name)
            println(" - " + compilers)
        this.project.provider{compilers.collectEntries { c-> this.getSourceEncodings(c).getOrElse([:]) }}
    }
    protected Provider<Map<File, String>> getSourceEncodings(TaskProvider compiler) {
        compiler.flatMap({this.getSourceEncodings(it)}).orElse([:])
    }
    protected Provider<Map<File, String>> getSourceEncodings(JavaCompile c) {
        project.provider({c.source.matching({include("**/*.java")}).collectEntries({[(it.canonicalFile): c.options.encoding]})})
    }
    protected Provider<Map<File, String>> getSourceEncodings(GroovyCompile c) {
        return project.provider({
            c.source.matching({include("**/*.java")}).collectEntries({[(it.canonicalFile): c.options.encoding]}) +
            c.source.matching({include("**/*.groovy")}).collectEntries({[(it.canonicalFile): c.groovyOptions.encoding]})
        })
    }
    protected Provider<Map<File, String>> getSourceEncodings(ScalaCompile c) {
        return project.provider({
            c.source.matching({include("**/*.java")}).collectEntries({[(it.canonicalFile): c.options.encoding]}) +
            c.source.matching({include("**/*.scala")}).collectEntries({[(it.canonicalFile): c.scalaCompileOptions.encoding]})
        })
    }
}
