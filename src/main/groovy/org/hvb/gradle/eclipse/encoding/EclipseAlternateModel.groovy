package org.hvb.gradle.eclipse.encoding

import javax.inject.Inject

import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.file.ConfigurableFileCollection
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Nested

public abstract class EclipseAlternateModel<T> extends Closure<T> {
    private Project project
    
    @Nested
    abstract public CustomEncoding getEncodings();

    @Inject
    public EclipseAlternateModel(Project project) {
        super(project, project)
        this.project = project
    }
    
    T doCall(@DelegatesTo(EclipseAlternateModel) final Closure<T> closure) {
        closure.delegate = this
        closure.call()        
    } 
    
    public void encodings(Action<? super CustomEncoding> action) {
        action.execute(getEncodings());
    }
}